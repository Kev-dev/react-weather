import React from 'react';
import { Sparklines, SparklinesLine, SparklinesReferenceLine } from 'react-sparklines';

export default function (props) {

  function average(data) {
    return Math.round(data.reduce((sum, val) => sum + val) / data.length);
  }

  return (
    <div>
      <Sparklines svgWidth={225} svgHeight={75} data={props.data}>
        <SparklinesLine color={props.color} />
        <SparklinesReferenceLine reference="avg" />
      </Sparklines>
      <div>{average(props.data)}</div>
    </div>
  );
}
