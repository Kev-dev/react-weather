import React, { Component } from 'react';
import Chart from '../components/chart';
import { connect } from 'react-redux'

class WeatherList extends Component {

  renderWeather(cityData) {
    const temps = cityData.list.map(weather => weather.main.temp);
    const pressure = cityData.list.map(weather => weather.main.humidity);
    const humidity = cityData.list.map(weather => weather.main.pressure);
    return (
      <tr key={cityData.city.name}>
        <td>
          {cityData.city.name}
        </td>
        <td>
          <Chart data={temps} color="red" />
        </td>
        <td>
          <Chart data={pressure} color="purple" />
        </td>
        <td>
          <Chart data={humidity} color="green" />
        </td>
      </tr>
    );
  }

  render() {
    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th>City</th>
            <th>Temperature (°C)</th>
            <th>Pressure (hPa)</th>
            <th>Humidity (%)</th>
          </tr>
        </thead>

        <tbody>
          {this.props.weather.map(this.renderWeather)}
        </tbody>

      </table>
    );
  }
}


function mapStateToProps(state) {
  return { weather: state.weather };
}

export default connect(mapStateToProps)(WeatherList);
