import axios from 'axios';

const API_KEY = 'a7c1e4c70f841fabd7de357a308639ab';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}&units=metric`

export function fetchWeather(city) {
  const url = `${ROOT_URL}&q=${city},be`;
  const request = axios.get(url);
  return {
    type: 'FETCH_WEATHER',
    payload: request
  };
}
